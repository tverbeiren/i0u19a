---
title: Data Processing
layout: page
excerpt: In this session, we discuss the distribution of data and computations. We discuss two implementations of map/reduce&#58; Hadoop and Spark.
---

# Exercise 1

Look up the meaning and function of the following libraries and tools:

* Impala
* Sqoop
* Oozie

**Q1. Describe how they fit into the Hadoop/Spark ecosystem.**


# Exercise 2

Think about finding the minimum and maximum in a dataset using a MapReduce approach. Find a way to do this using 1 iteration.

**Q2a. How would you tackle this? Describe the solution.**

**Q2b. How would you calculate an average using MapReduce?**

Make a drawing for yourself of the map and reduce phase.


# Exercise 3

In the exercises about RDBMS, we have created some top-10 lists. Look back how this was done.

**Q3a. Describe in words how a top-10 can be created using SQL language.**

In a MapReduce approach, we can also create a top-10. How can this be done?

**Q3b. Describe how a top-10 can be created using MapReduce. Think of the previous exercise.**

