---
title: Data Visualization Exercise Session
layout: page
excerpt: In this session, [processing](http://www.processing.org) is further introduced in order to create some visualizations later during the semester.
---